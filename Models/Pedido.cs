using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tech_test_payment_api.Models;

public class Pedido
{
    public int Id { get; set; }
    public int Quantidade { get; set; }

    [ForeignKey("Produto")]
    public int ProdutoId { get; set; }
    
}

