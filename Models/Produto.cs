using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Models
{
    public class Produto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}