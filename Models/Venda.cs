using System.ComponentModel.DataAnnotations.Schema;

namespace tech_test_payment_api.Models;

public class Venda
{
    public int Id { get; set; }
    public DateTime Data { get; set; }
    public EnumStatus Status { get; set; }

    [ForeignKey("Vendedor")]
    public int VendedorId { get; set; }

    [ForeignKey("Pedido")]
    public int PedidoId { get; set; }

}
