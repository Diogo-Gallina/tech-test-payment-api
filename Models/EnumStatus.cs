using System.ComponentModel;
using System.Reflection;

namespace tech_test_payment_api.Models;

public enum EnumStatus{
    AguardandoPagamento ,
    PagamentoAprovado ,
    EnviadoParaTransportadora ,
    Entregue ,
    Cancelada 
}

