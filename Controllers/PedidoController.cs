using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers;

[ApiController]
[Route("[controller]")]
public class PedidoController : ControllerBase
{
    private readonly OrganizadorContext _context;

    public PedidoController(OrganizadorContext context)
    {
        _context = context;
    }

        [HttpPost]
    public IActionResult Registrar(Pedido pedido){
        _context.Add(pedido);
        _context.SaveChanges();

        return CreatedAtAction(nameof(BuscarPorId), new {id = pedido.Id}, pedido);
    }

    [HttpGet ("{id}")]
    public IActionResult BuscarPorId(int id)
    {
        var pedido = _context.Pedidos.Find(id);

        if (pedido == null)
            return NotFound();

        return Ok(pedido);   
    }    

    [HttpGet("Obter Todos")]
    public IActionResult ObterTodos(){
        var pedido = _context.Pedidos.ToList();
        return Ok(pedido);
    }

    [HttpPut("{id}")]
    public IActionResult Atualizar(int id, Pedido pedido)
    {
        var pedidoBanco = _context.Pedidos.Find(id);

        pedidoBanco.Quantidade = pedido.Quantidade;
        pedidoBanco.ProdutoId = pedido.ProdutoId;


        _context.Update(pedidoBanco);
        _context.SaveChanges();

        return Ok(pedidoBanco);
    }

    [HttpDelete("{id}")]
    public IActionResult Deletar(int id)
    {
        var pedido = _context.Pedidos.Find(id);
        
        if (pedido != null)
        {
            _context.Pedidos.Remove(pedido);
            _context.SaveChanges();
        }
        
        return NoContent();
    }
}
