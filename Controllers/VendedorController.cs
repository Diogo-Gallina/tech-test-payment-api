using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers;

[ApiController]
[Route("[controller]")]
public class VendedorController : ControllerBase
{
    private readonly OrganizadorContext _context;

    public VendedorController(OrganizadorContext context)
    {
        _context = context;
    }


    [HttpPost]
    public IActionResult Registrar(Vendedor vendedor)
    {
        _context.Add(vendedor);
        _context.SaveChanges();

        return Ok(vendedor);
    }   
    
    [HttpGet("Obter Todos")]
    public IActionResult ObterTodos(){
        var vendedor = _context.Vendedores.ToList();
        return Ok(vendedor);
    } 

    [HttpPut("{id}")]
    public IActionResult Atualizar(int id, Vendedor vendedor)
    {
        var vendedorBanco = _context.Vendedores.Find(id);

        if(vendedorBanco == null)
            return NotFound();   

        vendedorBanco.Cpf = vendedor.Cpf;
        vendedorBanco.Email = vendedor.Email; 
        vendedorBanco.Nome = vendedor.Nome;
        vendedorBanco.Telefone = vendedor.Telefone;

        _context.Update(vendedorBanco);
        _context.SaveChanges();

        return Ok(vendedor);
    }

    [HttpDelete("{id}")]
    public IActionResult Deletar(int id)
    {
        var vendedor = _context.Vendedores.Find(id);
        
        if (vendedor != null)
        {
            _context.Vendedores.Remove(vendedor);
            _context.SaveChanges();
        }
        
        return NoContent();
    }

}
