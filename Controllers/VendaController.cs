using System.Net;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;
using Microsoft.AspNetCore.JsonPatch;

namespace tech_test_payment_api.Controllers;



[ApiController]
[Route("[controller]")]
public class VendaController : ControllerBase
{
    private readonly OrganizadorContext _context;

    public VendaController(OrganizadorContext context)
    {
        _context = context;
    }


    [HttpPost]
    public IActionResult Registrar(Venda venda){

        if(venda.Status != EnumStatus.AguardandoPagamento)
        {
            throw new NotImplementedException("A venda só pode ser registrada com o status 'Aguardando pagamento'");
        }

        _context.Add(venda);
        _context.SaveChanges();

        return CreatedAtAction(nameof(BuscarPorId), new {id = venda.Id}, venda);
    }

    [HttpGet ("{id}")]
    public IActionResult BuscarPorId(int id)
    {
        var venda = _context.Vendas.Find(id);

        if (venda == null)
            return NotFound();

        return Ok(venda);   
    }

    [HttpPut("{id}, {status}")]
    public IActionResult AtualizarStatus(int id, EnumStatus status)
    {
        var venda = _context.Vendas.Find(id);

        if(venda == null) return NotFound();

        if(venda.Status == EnumStatus.AguardandoPagamento && (status == EnumStatus.PagamentoAprovado || status == EnumStatus.Cancelada))
        {
            venda.Status = status;
        }
        else if(venda.Status == EnumStatus.PagamentoAprovado && (status == EnumStatus.EnviadoParaTransportadora  || status == EnumStatus.Cancelada))
        {
            venda.Status = status;
        }
        else if(venda.Status == EnumStatus.EnviadoParaTransportadora && status == EnumStatus.Entregue)
        {
            venda.Status = status;
        }
        else
        {
            throw new ("Essa alterção é invalida para o atual tipo de status");
        }

        _context.Vendas.Update(venda);
        _context.SaveChanges();

        return Ok(venda);
    }



    [HttpGet("Obter Todos")]
    public IActionResult ObterTodos(){
        var venda = _context.Vendas.ToList();
        return Ok(venda);
    }

    [HttpDelete("{id}")]
    public IActionResult Deletar(int id)
    {
        var venda = _context.Vendas.Find(id);
        
        if (venda != null)
        {
            _context.Vendas.Remove(venda);
            _context.SaveChanges();
        }
        
        return NoContent();
    }


}
