using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers;

[ApiController]
[Route("[controller]")]
public class ProdutoController : ControllerBase
{
    private readonly OrganizadorContext _context;

    public ProdutoController(OrganizadorContext context)
    {
        _context = context;
    }

    [HttpPost]
    public IActionResult Registrar(Produto produto)
    {
        _context.Add(produto);
        _context.SaveChanges();

        return CreatedAtAction(nameof(BuscarPorId), new {id = produto.Id}, produto);
    }

    [HttpGet ("{id}")]
    public IActionResult BuscarPorId(int id)
    {
        var produto = _context.Produtos.Find(id);

        if (produto == null)
            return NotFound();

        return Ok(produto);   
    }    

    [HttpGet("Obter Todos")]
    public IActionResult ObterTodos(){
        var produto = _context.Produtos.ToList();
        return Ok(produto);
    }

    [HttpPut("{id}")]
    public IActionResult Atualizar(int id, Produto produto)
    {
        var produtoBanco = _context.Produtos.Find(id);

        produtoBanco.Nome = produto.Nome;

        _context.Update(produtoBanco);
        _context.SaveChanges();

        return Ok(produtoBanco);
    }

    [HttpDelete("{id}")]
    public IActionResult Deletar(int id)
    {
        var produto = _context.Produtos.Find(id);
        
        if (produto != null)
        {
            _context.Produtos.Remove(produto);
            _context.SaveChanges();
        }
        
        return NoContent();
    }
}
